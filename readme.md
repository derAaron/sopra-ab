# Getting started

* nach dem clonen und jedem `git pull` ein `npm install` machen
* Bei OpenProject einloggen und den Session-Cookie besorgen (F12 -> Network -> Request-Headers). Sieht ungefähr so aus `_open_project_session=df207bsdghedrhrf867d86f5d8ba0a6e`
* `npm start <session_cookie>` lädt alle Tickets mit Work-Log herunter
* `npm run arbeitsaufstellung "<Vor und Nachname>"` erstellt einen Bericht über alle bearbeiteten Tickets und speichert ihn als markdown-tabelle unter `arbeitsaufstellung.md`
* `npm run sprintbericht "2017-04-1" "2017-04-30"` erstellt einen Bericht über diesen Zeitraum für alle Developer und speichert ihn als markdown-tabelle unter `arbeitsaufstellung.md`