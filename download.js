const syncRequest = require("sync-request");
const cheerio = require("cheerio")
require("./string_scan");
fs = require("fs");

const max_packages = 1000;

const session_cookie = process.argv[2];

getWorklogForPackages = function (result) {
    const array = JSON.parse(result);
    const packages = array._embedded.elements;
    let counter = 1;
    packages.map((package)=> {
        const worklog = syncRequest(`GET`, `https://openproject.imn.htwk-leipzig.de/work_packages/${package.id}/time_entries`,
            {
                headers: {
                    "Cookie": session_cookie,
                    "Accept": "*/*",
                    "Accept-Language": "de-DE,de;q=0.8,en-US;q=0.6,en;q=0.4",
                    "Cache-Control": "no-cache",
                    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.90 Safari/537.36 Vivaldi/1.91.867.38"
                }
            });
        if(worklog.statusCode !== 200)
            console.error(`Statuscode [${worklog.statusCode}] beim laden von Ticket [${package.id}]`);
        const $ = cheerio.load(worklog.getBody('utf8'));
        const raw_time_log_array = $("#content > script").html().scan(/\.timeEntries=(\[.*\]);gon\.project/g);
        package.work_log = raw_time_log_array[0][0] !== "" ? JSON.parse(raw_time_log_array[0][0]) : [];
        console.info(`Ticket ${counter++} of ${packages.length}`);
        return package;
    });

    fs.writeFile('op.json', JSON.stringify(packages, null, 2), function (err) {
        if (err) throw err;
        console.log('Saved!');
    });
}

const res = syncRequest(`GET`, `https://openproject.imn.htwk-leipzig.de/api/v3/projects/15/work_packages?columns[]=id&columns[]=subject&columns[]=type&columns[]=status&columns[]=assignee&columns[]=updatedAt&columns[]=estimatedTime&filters=[{"status":{"operator":"*","values":[]}}]&offset=1&pageSize=${max_packages}&showHierarchies=true&showSums=false&sortBy=[["id","asc"]]&timelineVisible=false&timelineZoomLevel=days`,
    {
        headers: {
            "Cookie": session_cookie
        }
    });
getWorklogForPackages(res.getBody('utf8'));
