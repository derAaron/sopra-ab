const fs = require("fs");
const _ = require("lodash");

const user_full_name = process.argv[2];
const outFile = `arbeitsaufstellung.md`;

fs.readFile("./op.json", (err, data) => {
    fs.writeFile(outFile, '');
    if (err) {
        console.error(err);
    }

    packages = JSON.parse(data);

    fs.appendFileSync(outFile, `| ID  | Arbeitspaket | Bereich | Startdatum | Enddatum |   h | Dokument in Git |
| ----:| ----------------- | ----------- | ------------ | ---------- | ---:| -------------------- |
`);

    packages.forEach((package)=> {
        const filtered = package.work_log.filter(f => f.user.name === user_full_name && f.work_package.id === package.id);
        filtered.reverse();
        if (filtered.length > 0) {
            const activities = _.uniq(filtered.map((m) => m.activity.name)).join(", ");
            fs.appendFileSync(outFile,`| ${package.id} | ${package.subject} | ${activities} | ${filtered[0].spent_on} | ${filtered[filtered.length - 1].spent_on} | ${filtered.reduce((sum, f) => sum + f.hours, 0)} |  |\n`);
        }
    });


    console.log('Saved!');
});