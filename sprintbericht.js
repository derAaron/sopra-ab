const fs = require("fs");

function toDate(dateStr) {
    const [year, month, day] = dateStr.split("-");
    return new Date(year, month - 1, day);
}

const start_date = toDate(process.argv[2]);
const end_date = toDate(process.argv[3]);
const outFile = `sprintbericht.md`;

fs.readFile("./op.json", (err, data) => {
    fs.writeFile(outFile, '');
    if (err) {
        console.error(err);
    }

    packages = JSON.parse(data);

    packages.forEach((package)=> {
        const filtered = package.work_log.filter(f => start_date <= new Date(f.spent_on)
                                                        && new Date(f.spent_on) <= end_date
                                                        && f.work_package.id === package.id);
        filtered.reverse();

        if (filtered.length > 0) {
            fs.appendFileSync(outFile, `

### Arbeitspaket: [${package.id}] ${package.subject}
**Arbeitszeit:** ${filtered.reduce((sum, f) => sum + f.hours, 0)} Stunde(n) **Zeitraum:** ${filtered[0].spent_on} bis ${filtered[filtered.length - 1].spent_on}

| Developer | Datum | Kommentar | h | Bereich |
| ---------------------- | ------------- | -------------------------------------------- | ---: | --------------- |
`);
            filtered.forEach((f) => {
                fs.appendFileSync(outFile, `| ${f.user.name} | ${f.spent_on} | ${f.comments} | ${f.hours} | ${f.activity.name} |\n`);
            });
        }
    });
});