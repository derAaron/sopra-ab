String.prototype.scan = function (regex) {
    if (!regex.global) throw "regex must have 'global' flag set";
    var r = []
    this.replace(regex, function () {
        r.push(Array.prototype.slice.call(arguments, 1, -2));
    });
    return r;
}